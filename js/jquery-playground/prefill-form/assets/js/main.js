
$( function() {

    let $submitArticleForm = $('#submit-article-form'),
        $list = $( '#list' ),
        $title = $( '#title', $submitArticleForm ), 
        $body = $( '#body', $submitArticleForm )[0],
        $getArticleForm = $('#get-article-form'),
        dataStorage = getDataFromLocalStorage() //stored data / data to be stored
        ;
        
        displayStoredArticles(dataStorage); //displaying articles from localStorage

        //pressing Submit button adds the article in the list, saves it in localStorage and clears the input fields 
        $submitArticleForm.on( 'submit', function( event ) {
            event.preventDefault();

            //creating an unique id
            let ids = getListIds();
            let localArticleId;
            if ( !ids ){ //list is empty for the moment, generating the first id, it will be 1
              //  console.log(ids);
                localArticleId = 1;
            }else { //list is not empty, generating an unique id
              //  console.log(ids.join(', '));
                localArticleId =  generateUniqueId(ids);
            }

            console.log(localArticleId);
            let tplArticle = `<li class="article col-md-12 text-center" data-id="${localArticleId}"><h2>${$($title).val()}</h2><p>${$($body).val()}</p><a href="" class="delete"><button>x</button></a><a href="" class="edit"><button>edit</button></a></li>`;
            $list.append(tplArticle);
            console.log(getListIds());
            //localStorage, creating the object to save
            let articleObj = {
                id: localArticleId,
                title: $($title).val(),
                body: $($body).val(),
            };
            if (!dataStorage || dataStorage.length == 0) { //nothing stored yet
                dataStorage = [];
            }
                readTheDataToBeStored(dataStorage);
                addArticleObjToDataToBeStored(articleObj, dataStorage);
                readTheDataToBeStored(dataStorage);
                storeData(dataStorage);
             
                //clears the unput fields 
                $($title).val("Type the title of your article here");
                $($body).val("Type your article here");
        });
        
        //pressing Auto button gets an article with a random id from https://jsonplaceholder.typicode.com
        $getArticleForm.on( 'submit', function( event ) {
            event.preventDefault();
         let id = Math.floor(Math.random() * 100);
         $.get(`https://jsonplaceholder.typicode.com/posts?id=${id}`)
         .done(function (response) {
             console.log(response);
             $($title).val(`${response[0].title}`);
             $($body).val(`${response[0].body}`);
         })
         .fail(function (data) {
             console.log('failed');
         })
         ; //get 
            
        });

       //pressing edit button allows to edit the article in the list 
        $list.on( 'click', 'a.edit', function( event ) {
            event.preventDefault();
            console.log($(this).parent().children("h2").text());
            console.log($(this).parent().children("p").text()); //!!!!
            //makes fields editable
            let tplArticle = `<li class="article col-md-12 text-center" data-id="${$(this).parent().attr("data-id")}">
             <input type="text" id="title-edit" name="title-edit" class="form-control my-1 mr-sm-2" value="${$(this).parent().children("h2").text()}" />
            <textarea id="body-edit" name="body-edit" class="form-control my-1 mr-sm-2" rows="4" cols="50">
            ${$(this).parent().children("p").text()}
        </textarea>
            <a href="" class="delete"><button>x</button></a>
            <a href="" class="save"><button>save</button></a></li>`;
            $(this).parent().replaceWith(tplArticle);
            //will continue in 'save'
        } );

        //pressing 'Save' button saves the modifications in the list and localStorage
        $list.on( 'click', 'a.save', function( event ) {
            event.preventDefault();
            //makes fields text, textarea
            let id = $(this).parent().attr("data-id"),  // :))))
                title = $(this).parent().children("input").val(),
                body = $(this).parent().children("textarea").val(),
                tplArticle = `<li class="article col-md-12 text-center" data-id="${$(this).parent().attr("data-id")}">
            <h2>${title}</h2>
            <p>${body}</p>
            <a href="" class="delete"><button>x</button></a>
            <a href="" class="edit"><button>edit</button></a></li>`
            ;

            //for the localStorage: creating the object that will replace the one with the given id
                let articleObj = {
                id: id,
                title: title,
                body: body,
                };
            $(this).parent().replaceWith(tplArticle);
            
            //updating the data to be stored
            dataStorage = updateStoredArticle(articleObj, dataStorage);
            storeData(dataStorage);
        } );

         //pressing x button deletes the article from the list and from the localstorage
        $list.on( 'click', 'a.delete', function( event ) {
            event.preventDefault();
            $(this).parent()[0].remove();
            dataStorage = removeStoredArticle($(this).parent().attr("data-id"), dataStorage);
            storeData(dataStorage);
        } );


        //generates an unique id
        function generateUniqueId(ids){
           let id = Math.floor(Math.random() * 1000);
            for (let i = 0; i< ids.length; i++){
                if (id == Number(ids[i]))
                {
                    generateUniqueId(ids);
                }
            }
            return id;
        }

         //gets the value of all the ids in the list in an array
         //used by  generateUniqueId(ids)
        function getListIds(){
            let ids = [ ];
            if  ( $list.has('li').length === 0){ //the list is empty
                return null;
            }else {
                $( 'li.article' ).each(function() {
                    console.log($(this).data('id'));  
                    ids.push($(this).data('id'));
                });
                return ids;
            }
        }

     
        function addArticleObjToDataToBeStored(articleObjToAdd, dataToStore){
            dataToStore.push(articleObjToAdd);
        }

        //saves data on localstorage
        function storeData(dataToStore){
            localStorage.setItem("articles",JSON.stringify(dataToStore));
        }

        //logs each item of the array
        function readTheDataToBeStored(dataToStore){
            console.log("Data:");
            dataToStore.forEach(obj => {
                console.log(obj);
            });
            console.log("____________________________");
        }

        //gets data from localStorage
        function getDataFromLocalStorage(){
            return JSON.parse(localStorage.getItem("articles"));    
        }

        //displays the articles that are on localStorage
        function displayStoredArticles(articles){
            if (!articles || articles.length == 0) { //nothing stored yet
                return; //nothing to display
            }
            articles.forEach(article => {
            let tplArticle = `<li class="article col-md-12 text-center" data-id="${article.id}"><h2>${article.title}</h2><p>${article.body}</p><a href="" class="delete"><button>x</button></a><a href="" class="edit"><button>edit</button></a></li>`;
            $list.append(tplArticle);
            });
        }

        //removes the article with the given id from a dataset 
        //returns the updated dataset, an array with the articles that remain (to be saved in the localStorage)
        function removeStoredArticle(id, data){
            console.log(id);
            data = data.filter(function(article) { 
                return article.id != id; //https://alligator.io/js/filter-array-method/
            } ); //keeps all the articles without the one with the given id
            //readTheDataToBeStored(data);
            return data;
        }

        //updates the article with the given id from a dataset 
        //returns the the updated dataset, an array (to be saved in the localStorage)
        function updateStoredArticle(article, data){
            console.log(article.id + ' ' + article.title + ' ' + article.body);
            data.find((a) => {
                if (a.id == article.id)
                {
                    a.title = article.title;
                    a.body = article.body;
                    return;
                }
            });
           //readTheDataToBeStored(data);
            return data;
        }

    } );