# Formulaire pré rempli

Cet exercice doit être réalisé en jQuery, avec Bootstrap et la css doit être générée à partir du sass.
La page doit être responsive (utiliser Bootstrap pour ça, pas de media queries nécessaire)

## Dans l'HTML

- Créer un formulaire avec 4 éléments : un champ (input) Titre, un champ (texarea) Texte, un bouton Envoyer et un bouton Auto
- Créer une liste vide en dessous du formulaire (celle affiche ses items à l'envers)

## Dans le JS

- Cibler votre formulaire
- Cibler vos champs 
- Cibler vos boutons
- Cibler votre liste
- Ajouter un écouteur sur le submit du formulaire
- Cet écouteur doit stoper le chargement de la page
- Puis il doit créer un item dans la liste en dessous avec un title et un body (h2 et p) (utilisez prepend pour ajouter l'item au début de la liste)
- Ajouter un écouteur sur le click sur le bouton Auto
- Cet écouteur doit faire une requête sur https://jsonplaceholder.typicode.com/posts
- Au retour de la réponse, choisir un post au hasard parmis la totalité des posts
- Puis préremplir les champs Titre et Texte

## Bonus

- Chaque item de liste doit pouvoir être supprimé avec l'aide d'un lien 'x'