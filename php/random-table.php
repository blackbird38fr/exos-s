<?php
$persons = ['Maude', 'Otilia', 'Viviane', 'Lea', 'Esther', 'Esma', 'Laurie', 'Marion', 'Grace', 'Isa', 'Nathalie', 'Hayette', 'Florence', 'Fatima', 'Syriane', 'Sixtine'];
$nPersons = count($persons);
$nTables = 4;
$tables = []; //will contain an array of arrays

while (($nPersons > 0) && ($nTables > 0)){
    $table = array_rand(array_flip($persons), (int)($nPersons/$nTables));
    $persons = array_diff($persons, $table);
    $nPersons -=  (int)($nPersons/$nTables);
    $nTables -= 1;
    array_push($tables, $table);
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Random Tables</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- UIkit CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.2.0/css/uikit.min.css" />

    <!-- UIkit JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.2.0/js/uikit.min.js" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.2.0/js/uikit-icons.min.js" defer></script>
    <!-- FontAwesome -->
    <script src="https://use.fontawesome.com/releases/v5.3.1/js/all.js" defer></script>
</head>

<body class="uk-background-primary">

<div><h1 class="uk-text-center uk-background-default">Random Table</h1></div>
<hr class="uk-divider-icon">
<div class="uk-container "> <!-- uk-child-width-expand@s -->
    <div class="uk-child-width-1-1@s uk-child-width-1-2@m uk-text-center" uk-grid>
        <?php foreach ($tables as $key=> $table) { ?>
            <div class="uk-card uk-card-default uk-card-hover">
                <div class="uk-card-header uk-text-lead  uk-text-large">
                    <i class="fas fa-users uk-align-left"></i> <h1 class="uk-card-title uk-align-left">Table <?php echo $key+1; ?></h1>
                </div>
                <div class="uk-card-body uk-text-large">
                    <?php  foreach ($table as $key2 => $person) {
                        echo $person; echo ($key2 < count($table)-1) ? ', ' : ' ';
                    }?>
                </div>
                <div class="uk-card-footer"></div>
            </div>
        <?php } //for1?>
    </div>
</div>
<hr class="uk-divider-icon">
</body>
</html>